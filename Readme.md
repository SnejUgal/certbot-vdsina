# ACME DNS Challenger for VDSina

Use DNS challenges to get wildcard certificates from Let's Encrypt with VDSina.

## Requirements

You only need to have Python 3.11 installed on your server.

## Configuration

Before running, please create a configuration file `config.toml` in this
repository:

```toml
api_token = "..."
delay = 00:10:00

[domains]
"example.com" = ...
```

`api_token` must contain VDSina's API token for the control panel. You can
obtain it at <https://cp.vdsina.ru/user/list>. You can use your own token,
but it's recommended to create a new user with the following permissions:

- **Пользователь:** Панель управления, Настройки пользователя
- **Услуга:** Просмотр услуги
- **Запись DNS:** Список записей DNS, Создание записи DNS, Удаление записи DNS

This list of permissions was collected empirically. If you don't grant any of
the above permissions, the script may not succeed.

The `delay` field is optional and configures the amount of time for which the
auth hook sleep before the challenge is checked. Usually, 10 minutes is enough
for DNS changes to propagate to Let's Encrypt's servers, but may be increased
if needed.

In the `[domains]` section, you need to provide a list of domains which you
manage via VDSina. The domain name must be quoted. On the right side, you must
provide the ID of the DNS service, which can be seen at
<https://cp.vdsina.ru/dns/list>. For example, if you see a service named `DNS
#123456 — example.com`, then you should add `"example.com" = 123456` to the
section.

## Using

First, clone the repo anywhere on your server. Second, provide a configuration
file as described [above](#configuration).

When getting the certificate for the first time, you should provide manual
hooks:

```bash
certbot certonly --preferred-challenges dns --manual \
    --manual-auth-hook $path_to_repo/create_record.py \
    --manual-cleanup-hook $path_to_repo/cleanup_records.py \
    --domains $your_domains
```

Replace `$path_to_repo` with the path to this cloned repository. Provide your
domains instead of `$your_domains`. Subdomains are automatically managed by the
script.
