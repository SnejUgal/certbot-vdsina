from typing import Literal, Self, List
from urllib.request import Request, urlopen
import json

from api_types import ServiceId, DnsRecordId, DnsRecord, DnsRecordType
from config import api_token

Method = Literal["GET", "POST", "PUT", "DELETE"]

API_BASE_URL = "https://userapi.vdsina.ru/v1"

def make_request(method: Method, endpoint: str, **data) -> dict:
    request = Request(
        f"{API_BASE_URL}/{endpoint}",
        method=method,
        headers={ "Authorization": api_token },
        data=json.dumps(data).encode() if len(data) > 0 else None,
    )

    with urlopen(request) as response:
        parsed_response = json.loads(response.read().decode())
        if parsed_response["status"] != "ok":
            raise Exception(
                f"Received error in response: {parsed_response['description']}"
            )
        return parsed_response["data"]


def create_dns_record(
    service_id: ServiceId, *,
    host: str,
    record_type: DnsRecordType,
    value: str,
):
    make_request(
        "POST", f"dns.record/{service_id}",
        host=host, type=record_type, value=value,
    )

def get_dns_records(service_id: ServiceId) -> List[DnsRecord]:
    records = make_request("GET", f"dns.record/{service_id}")
    return [DnsRecord.from_dict(record) for record in records]

def delete_dns_record(dns_record_id: DnsRecordId):
    make_request("DELETE", f"dns.record/{dns_record_id}")
