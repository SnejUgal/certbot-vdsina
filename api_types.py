from typing import NewType, Literal, Self
from dataclasses import dataclass

ServiceId = NewType("ServiceId", int)
DnsRecordId = NewType("DnsRecordId", int)

DnsRecordType = Literal["A", "AAAA", "CNAME", "NS", "TXT"]

@dataclass
class DnsRecordPermissions:
    delete: bool

    @staticmethod
    def from_dict(raw: dict) -> Self:
        return DnsRecordPermissions(delete=raw["delete"])

@dataclass
class DnsRecord:
    id: DnsRecordId
    host: str
    can: DnsRecordPermissions

    @staticmethod
    def from_dict(raw: dict) -> Self:
        return DnsRecord(
            id=DnsRecordId(raw["id"]),
            host=raw["host"],
            can=DnsRecordPermissions.from_dict(raw["can"]),
        )
