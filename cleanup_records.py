#!/usr/bin/env python3.11

from os import environ

import config
import api

if __name__ == "__main__":
    domain = environ["CERTBOT_DOMAIN"]
    service_id, _ = config.get_service_id(domain)

    for record in api.get_dns_records(service_id):
        if not record.can.delete or record.host != f"_acme-challenge.{domain}.":
            continue

        api.delete_dns_record(record.id)
