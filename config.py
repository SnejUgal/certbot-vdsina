import tomllib
from typing import Tuple, Optional, NewType, Dict
from pathlib import Path

from api_types import ServiceId

DomainsDict = Dict[str, "DomainsDict"]
executing_directory = Path(__file__).parent

with open(executing_directory / "config.toml", "rb") as config_file:
    config = tomllib.load(config_file)

api_token: str = config["api_token"]

delay: float = 10.0 * 60
if "delay" in config:
    user_delay = config["delay"]
    delay = (
        user_delay.hour * 3600
        + user_delay.minute * 60
        + user_delay.second
        + user_delay.microsecond / 10e6
    )

domains: DomainsDict = {}
for domain, service_id in config["domains"].items():
    previous_level = domains
    for level in reversed(domain.split(".")):
        if level not in previous_level:
            previous_level[level] = {}
        previous_level = previous_level[level]
    previous_level["."] = ServiceId(service_id)

def get_service_id(domain: str) -> Tuple[ServiceId, Optional[str]]:
    levels = reversed(domain.split("."))
    subdomain = None

    previous_level = domains
    for level in levels:
        if level not in previous_level:
            subdomain = level
            for sublevel in levels:
                subdomain = f"{sublevel}.{subdomain}"
            break
        previous_level = previous_level[level]

    if "." not in previous_level:
        raise Exception(f"Unknown domain name: {domain}")
    return previous_level["."], subdomain
