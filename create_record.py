#!/usr/bin/env python3.11

import config
import api
from os import environ
from time import sleep

if __name__ == "__main__":
    domain = environ["CERTBOT_DOMAIN"]
    validation = environ["CERTBOT_VALIDATION"]
    remaining_challenges = int(environ["CERTBOT_REMAINING_CHALLENGES"])

    service_id, subdomain = config.get_service_id(domain)
    host = "_acme-challenge"
    if subdomain is not None:
        host += f".{subdomain}"

    api.create_dns_record(
        service_id,
        host=host, record_type="TXT", value=validation,
    )

    if remaining_challenges == 0:
        sleep(config.delay)
